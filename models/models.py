from datetime import datetime
from resources import db
import datetime
import pandas as pd


class Verificacao(db.Model):
    __tablename__ = 'verificacao'
    
    verificacao_uid = db.Column(db.Integer(), primary_key=True, unique=True)
    contrato_uid = db.Column(db.String(64), db.ForeignKey('contrato.contrato_uid'))
    clausula_uid = db.Column(db.Integer(), db.ForeignKey('clausula.clausula_uid'))
    indice_diff = db.Column(db.Float())
    resultado = db.Column(db.String(64))
    num_paragrafo = db.Column(db.Integer())
    paragrafo_correspondente = db.Column(db.String(3060))
    paragrafo_modificado = db.Column(db.String(3060))


    def __init__(self, verificacao_uid, contrato_uid, clausula_uid, indice_diff, resultado, num_paragrafo, paragrafo_correspondente, paragrafo_modificado):
        super().__init__()
        self.verificacao_uid = verificacao_uid
        self.contrato_uid = contrato_uid
        self.clausula_uid = clausula_uid
        self.indice_diff = indice_diff
        self.resultado = resultado
        self.num_paragrafo = num_paragrafo
        self.paragrafo_correspondente = paragrafo_correspondente
        self.paragrafo_modificado = paragrafo_modificado
    

    def save_verification(self):
        db.session.add(self)
        db.session.commit()

    def update_verification(self, resultado, paragrafo_modificado):
        self.resultado = resultado
       
        self.paragrafo_modificado = paragrafo_modificado

    @classmethod
    def find_verification(cls, contrato_uid, clausula_uid):
        contrato = cls.query.filter_by(
            contrato_uid=contrato_uid, clausula_uid=clausula_uid).first()
        if contrato:
            return contrato
        return None


class Contrato(db.Model):
    __tablename__ = "contrato"

    contrato_uid = db.Column(db.String(), primary_key=True, unique=True)
    valor_hash = db.Column(db.String(64))
    nome = db.Column(db.String(255))
    produto = db.Column(db.String(255))
    emissor = db.Column(db.String(255))
    nome_guerra = db.Column(db.String(255))
    tipo = db.Column(db.String(255))
    data_criacao = db.Column(db.DateTime)
    autor_criacao = db.Column(db.String(255))
    data_modificacao = db.Column(db.DateTime)
    autor_modificacao = db.Column(db.String(255))
    data_verificacao = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    total_clausulas = db.Column(db.Integer())
    num_clausulas_iguais = db.Column(db.Integer())
    num_clausulas_similares = db.Column(db.Integer())
    num_clausulas_diferentes = db.Column(db.Integer())

    def __init__(self, contrato_uid, valor_hash, nome, produto, emissor, nome_guerra, tipo, data_criacao, autor_criacao, data_modificacao, autor_modificacao, data_verificacao, total_clausulas, num_clausulas_iguais, num_clausulas_similares, num_clausulas_diferentes):
        self.contrato_uid = contrato_uid 
        self.valor_hash = valor_hash
        self.nome = nome
        self.produto = produto
        self.emissor = emissor     
        self.nome_guerra = nome_guerra
        self.tipo = tipo
        self.data_criacao = data_criacao        
        self.autor_criacao= autor_criacao
        self.data_modificacao = data_modificacao
        self.autor_modificacao = autor_modificacao
        self.data_verificacao = data_verificacao
        self.total_clausulas = total_clausulas
        self.num_clausulas_iguais = num_clausulas_iguais
        self.num_clausulas_similares = num_clausulas_similares
        self.num_clausulas_diferentes = num_clausulas_diferentes
    
    def json(self):
        return {
            'contrato_uid': self.contrato_uid,
            'valor_hash': self.valor_hash,
            'nome': self.nome,
            'produto': self.produto,
            'emissor': self.emissor,
            'nome_guerra':  self.nome_guerra,
            'tipo': self.tipo,
            'data_criacao': self.data_criacao,
            'autor_criacao': self.autor_criacao,
            'data_modificacao': self.data_modificacao,
            'autor_modificacao': self.autor_modificacao,
            'data_verificacao': self.data_verificacao,
            'total_clausulas': self.total_clausulas,
            'num_clausulas_iguais': self.num_clausulas_iguais,
            'num_clausulas_similares': self.num_clausulas_similares,
            'num_clausulas_diferentes': self.num_clausulas_diferentes
        }

    @classmethod
    def load_contracts(self):
        select = db.session.query(Contrato).statement
        contracts_df = pd.read_sql_query(select, db.engine)
        return contracts_df

    def load_contract(uid):
        select = db.session.query(Contrato).filter_by(contrato_uid=uid).statement
        contracts_df = pd.read_sql_query(select, db.engine)
        return contracts_df
    
    def find_contract_hash(valor_hash):

        select = db.session.query(Contrato).filter_by(valor_hash=valor_hash).statement
        contracts_df = pd.read_sql_query(select, db.engine)
        if len(contracts_df) > 0:
            return True
        else:
            return False

    def find_contract(contrato_uid):
        contrato = db.session.query(Contrato).filter_by(contrato_uid = contrato_uid ).first()
        if contrato:
            return contrato
        return None

    def save_contract(self):
        db.session.add(self)
        db.session.commit()

    def update_contract(self, contrato_uid, valor_hash, nome, produto, emissor, nome_guerra, tipo, data_criacao, autor_criacao, data_modificacao, autor_modificacao, data_verificacao, total_clausulas, num_clausulas_iguais, num_clausulas_similares, num_clausulas_diferentes):
        self.contrato_uid = contrato_uid 
        self.valor_hash = valor_hash
        self.nome = nome
        self.produto = produto
        self.emissor = emissor     
        self.nome_guerra = nome_guerra
        self.tipo = tipo
        self.data_criacao = data_criacao        
        self.autor_criacao= autor_criacao
        self.data_modificacao = data_modificacao
        self.autor_modificacao = autor_modificacao
        self.data_verificacao = data_verificacao
        self.total_clausulas = total_clausulas
        self.num_clausulas_iguais = num_clausulas_iguais
        self.num_clausulas_similares = num_clausulas_similares
        self.num_clausulas_diferentes = num_clausulas_diferentes        

    def update_contract_verifications(self, total_clausulas, num_clausulas_iguais, num_clausulas_similares, num_clausulas_diferentes, data_verificacao):
        self.total_clausulas = total_clausulas
        self.num_clausulas_iguais = num_clausulas_iguais
        self.num_clausulas_similares = num_clausulas_similares
        self.num_clausulas_diferentes = num_clausulas_diferentes   
        self.data_verificacao = data_verificacao


    def delete_contract(self):
            db.session.delete(self)
            db.session.commit()


class Clausula(db.Model):
    __tablename__ = "clausula"

    clausula_uid = db.Column(db.Integer(), primary_key=True, unique=True)
    clausula = db.Column(db.String(3060))
    tipo = db.Column(db.String(255))

    def __init__(self, clausula_uid, clausula, tipo):
        self.clausula_uid = clausula_uid
        self.clausula = clausula
        self.tipo = tipo
    
    def json(self):
        return {
            'clausula_uid': self.clausula_uid,
            'clausula': self.clausula,
            'tipo':self.tipo
        }

    def load_clauses():
        select = db.session.query(Clausula).statement
        clauses_df = pd.read_sql_query(select, db.engine)
        return clauses_df

    @classmethod
    def find_clause(cls, uid):
        clause = cls.query.filter_by(clausula_uid=uid).first()
        if clause:
            return clause
        return None

    def save_clause(self):
        db.session.add(self)
        db.session.commit()

    def update_clause(self, clausula, tipo):
        self.clausula = clausula
        self.tipo = tipo

    def delete_clause(self):
        db.session.delete(self)
        db.session.commit()
