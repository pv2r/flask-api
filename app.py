from resources import app, db
from flask_restful import Api
from resources.verificacao import Verificacoes, Verificacao_uid
from resources.contrato import Contratos, Contratos__all
from resources.operacao import Operacao_all, Operacao
from resources.clausulas import ClausulasAll, Clausulas
from resources.gerador_docx import Save_Sugetions

api = Api(app)
db.init_app(app)

@app.route("/", methods=["GET"])
def Home():
    print(app.config)
    return "HOME"

api.add_resource(Verificacoes, '/verificacoes')
api.add_resource(Contratos__all, '/contratos')
api.add_resource(Contratos,'/contratos/<string:uid>')
api.add_resource(Operacao_all, '/operacoes')
api.add_resource(Operacao, '/operacoes/<string:uid_contrato>')
api.add_resource(ClausulasAll, '/clausulas')
api.add_resource(Clausulas,'/clausulas/<string:uid>')
api.add_resource(Verificacao_uid, '/verificacoes/<string:uid>')
api.add_resource(Save_Sugetions, '/salvar-versao')


if __name__ == "__main__":
    app.run()
