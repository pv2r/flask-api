from datetime import datetime
import os
import hashlib
import docx
import re
import string
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import difflib
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import nltk
import pandas as pd
from flask import request
import math
import unicodedata
from uuid import uuid4
from models.models import Contrato

######################################################################
# 0.1 Paginação
######################################################################

def get_paginated_df(results, page, per_page):
    total = len(results)
    from_ = min((page * per_page - per_page) + 1, total)
    last_page = math.ceil(total/per_page)
    to = min(from_ + per_page - 1, total)
    data = results[from_ - 1: to]

    if page > last_page:
        from_ = None
        to = None
        data = None

    output = {}
    output['total'] = total
    output['current_page'] = page
    output['last_page'] = last_page
    output['from'] = from_
    output['to'] = to
    output['data'] = data

    return output

def get_paginated_df_complete(results, url, start, limit):
    start = int(start)
    limit = int(limit)
    count = len(results)
    last_page = math.ceil(count/limit)
   
      
    # make response

    obj = {}
    obj['from'] = start

    obj['first_page_url'] = url

    obj['per_page'] = limit
    obj['total'] = count
    obj['last_page'] = last_page
    # make URLs
    # current page
    obj['current_page'] = '%s' % (request.args.get('start', 1))

    # make previous url
    if start == 1:
        obj['previous_page'] = None
    else:
        start_copy = max(1, start - limit)
        limit_copy = start - 1
        obj['previous_page'] = start_copy
    # make next page url
    if start + limit > count:
        obj['next_page'] = None
        obj['to'] = count
    else:
        start_copy = start + limit
        obj['next_page'] = start_copy

    if limit >= count:
        limit = count
        # last page url
        obj['last_page'] = last_page
        obj['to'] = count
    else:
        obj['to'] = limit+start-1
        obj['last_page_url'] = url + \
            '?start=%d&limit=%d' % (count - limit + 1, last_page)

    # finally extract result according to bounds
    obj['data'] = results[(start - 1):(start - 1 + limit)]
    return obj

def get_paginated_list(DADOS, PAGE, PER_PAGE, TOTAL):
    TO = (PAGE*PER_PAGE - PER_PAGE) + PER_PAGE
    FROM = (PAGE*PER_PAGE - PER_PAGE)+1

    LAST_PAGE = math.ceil(TOTAL/PER_PAGE)

    obj = {}
    obj['from'] = FROM
    if TO > TOTAL:
        obj['to'] = TOTAL
    else:
        obj['to'] = TO
    obj['current_page'] = PAGE
    obj['per_page'] = PER_PAGE
    obj['total'] = TOTAL
    obj['dados'] = DADOS
    obj['last_page'] = LAST_PAGE
 
    return obj



#######################################################################
# 1. Extração
#######################################################################

def get_text_name(folder):
    '''
        Recebe o caminho de um diretório e retorna uma lista com os nomes dos arquivos de extensão .docx ordenados
    '''

    only_docx = [f for f in os.listdir(folder) if f.lower().endswith('.docx')]
    only_docx.sort()
    return only_docx


def find_file(name, path):
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)


def get_text_dir(folder):
    '''
        Recebe o caminho de um diretório e retorna uma lista com os caminhos dos arquivos de extensão .docx ordenados
    '''

    only_docx_path = [os.path.join(folder, f) for f in os.listdir(folder) if f.lower().endswith('.docx')]
    only_docx_path.sort()
    return only_docx_path


def get_hash(file_dir, chunk_size=1024):
    '''
        Recebe o caminho de um arquivo e retorna o valor_hash acumulado
    '''

    valor_hash = hashlib.sha256()
    with open(file_dir, "rb") as f:
        # lê o primeiro bloco do arquivo
        chunk = f.read(chunk_size)
        # fica lendo arquivo até o fim e atualiza o valor_hash
        while chunk:
            valor_hash.update(chunk)
            chunk = f.read(chunk_size)

    # Returna hex checksum
    return valor_hash.hexdigest()


def para_text_extract(files):
    '''
        Extrai docs, parágrafos docx e textos separados em parágrafos docx e '\n'
    '''

    docs = [docx.Document(doc) for doc in files]
    texts = [doc.paragraphs for doc in docs]

    para_texts = []
    for text in texts:
        para_group = []
        for para in text:
            para_text_normalized = unicodedata.normalize('NFKC', para.text)
            para_group.extend(para_text_normalized.split('\n'))
        para_texts.append(para_group)

    return docs, texts, para_texts


def text_info(para_texts):
    '''
        Extrai número de documentos, tamanho de cada documento e tamanho total dos documentos
    '''

    n_docs = len(para_texts)
    docs_len = [len(para) for para in para_texts]
    total_len = sum(docs_len)

    return n_docs, docs_len, total_len


def date_extract(docs):
    '''
        Extrai datas dos cabeçalhos
    '''

    dates = []
    for doc in docs:
        section = doc.sections[0]
        header = section.first_page_header
        try:
            date = header.paragraphs[1].text
        except IndexError:
            date = None
        dates.append(date)

    return dates


def full_text_extract(filename):
    '''
        Extrai texto completo
    '''

    doc = docx.Document(filename)

    full_text = []
    for para in doc.paragraphs:
        full_text.append(para.text)

    return '\n'.join(full_text)


def combine_text(list_of_strings):
    '''
        Une lista de strings com caractere espaço
    '''

    combined_text = ' '.join(list_of_strings)
    return combined_text


def csv_to_df(path):
    '''
        Cria lista e dataframe de cláusulas a partir do arquivo csv
    '''

    with open(path) as f:
        clauses = []
        for line in f:
            clauses.append(line.rstrip("\n"))

    # cria dataframe das cláusulas
    clauses_df = pd.DataFrame({'clausulas': clauses})
    return clauses, clauses_df


#######################################################################
# 2. Pré-processamento
#######################################################################

def clean_text_round1(text):
    '''
        Elimina pontuação, colchetes e palavras que contribuam pouco
    '''

    text = text.lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub('[%s]' %re.escape(string.punctuation),'', text)
    text = re.sub('\w*\d\w*', '', text)
    return text


def clean_text_round2(text):
    '''
        Segunda rodada de limpeza
    '''
    
    text = re.sub('[‘’“”…]', '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\d{2}.\d{3}.\d{3}/\d{4}-\d{2}','"CNPJ"', text)
    text = re.sub('\(s\)','', text)
    text = re.sub('\(\w{2}\)','', text)
    return text


def tokenize(para_text):
    '''
        Tokeniza em palavras, coloca em minúsculas, mantém apenas tokens alfanuméricos e remove stopwords a partir de lista de parágrafos.
        Para ser usada com pd.DataFrame(df['coluna'].apply(tokenize))
    Arg:
        Lista de parágrafos
    Return:
        Lista de lista de tokens
    '''
    stops = stopwords.words('portuguese')
    stops.append('s')

    para_group = []
    for para in para_text:
        lower_tokens = word_tokenize(para.lower())
        alnum_tokens = [t for t in lower_tokens if t.isalnum()]
        no_stops = [t for t in alnum_tokens if t not in stops]
        para_group.append(no_stops)

    return para_group


def para_tokenize(paragraph):
    stops = stopwords.words('portuguese')
    stops.append('s')

    lower_tokens = word_tokenize(paragraph.lower())
    alnum_tokens = [t for t in lower_tokens if t.isalnum()]
    no_stops = [t for t in alnum_tokens if t not in stops]

    return no_stops


#######################################################################
# 3. Processamento
#######################################################################

def similarity(clauses, text):
    '''
        Calcula a similaridade entre uma lista de cláusulas e uma lista de segmentos de texto e
        retorna um array com o índice de similaridade entre cada cláusula e cada segmento
    Args:
        clauses - lista de strings contendo cláusulas obrigatórias
        text - lista de parágrafos de um texto
    Return:
        Array com índices de similaridade entre cada cláusula e cada segmento
    '''

    similarity_indices = []
    for clause in clauses:
        similarity_group = []
        for para in text:
            ratio = difflib.SequenceMatcher(None, clause, para).ratio()
            similarity_group.append(ratio)
        similarity_indices.append(similarity_group)
    return np.array(similarity_indices)


# cria alias da função similarity para compatibilidade
similarities = similarity


def included(similarity_indices, cut=0.8):
    '''
        Gera uma tupla de listas indicando se cada parágrafo contém uma das cláusulas
        e se cada cláusula está presente no texto
    Arg:
        similarity_indices - Lista com os índices de similaridade da clásula em cada parágrago do texto
    Return:
        lines - Lista indicando se cada parágrafo contém uma das cláusulas
        columns - Lista se cada cláusula está presente no texto
    '''

    mask = similarity_indices >= cut
    lines = np.any(mask, axis=0)
    columns = np.any(mask, axis=1)

    return lines, columns


# função para compatibilidade (remover após revisar)
def included_clauses(similarity_indices, cut=0.8):
    '''
        Gera uma lista indicando se as cláusulas estão presentes em cada parágrafo do texto
    Arg:
        similarity_indices - Lista com os índices de similaridade da clásula em cada parágrago do texto
    Return:
        Lista com 0s e 1s indicando se a cláusula está presente em cada parágrafo do texto
    '''

    mask = similarity_indices >= cut
    #print(mask)
    n_lines = np.any(mask,axis=1)
    lines = np.any(mask, axis=0)
    return lines, n_lines


def included_clauses_class(similarity_indices, cut=0.8):
    mask_igual = (similarity_indices == 1)
    mask_similar = np.logical_and(similarity_indices >= cut, similarity_indices < 1)
    mask_diferente = similarity_indices < cut
    igual = np.any(mask_igual, axis=1)
    similar = np.any(mask_similar, axis=1)
    diferente = np.alltrue(mask_diferente, axis=1)

    return igual, similar, diferente


def get_position(similarity_indices, cut=0.8):
    para_position = []
    for i in similarity_indices:
        line = np.where(i >= cut)
        if line[0].size > 0:
            para_position.append(line[0][0])
        else:
            para_position.append(np.nan)

    return para_position


def get_indices(similarity_indices):
    max_similarity_indices = []
    for i in similarity_indices:
        line = np.max(i)
        max_similarity_indices.append(line)

    return max_similarity_indices


def build_clauses_df(included_df, clauses, included_clauses, igual, similar, id_contrato, diferente, para_position, indices, clausulas_uid):
    clausulas_df = pd.DataFrame({'clausula': clauses,
                                'incluidas_diff': included_clauses,
                                'igual': igual,
                                'similar': similar,
                                 'contrato_uid': id_contrato,
                                'diferente': diferente,
                                 'num_paragrafo': para_position,
                                 'indice_diff': indices})

    clausulas_df['resultado'] = pd.Series(np.where(clausulas_df.loc[:, ['igual', 'similar', 'diferente']].any(
        axis=1), clausulas_df.loc[:, ['igual', 'similar', 'diferente']].idxmax(axis=1), np.nan), name="result")

    clausulas_df.drop(columns=['incluidas_diff', 'igual', 'similar', 'diferente'], inplace=True)
    
    clausulas_df['clausula_uid'] = clausulas_uid

    merged_df = pd.merge(clausulas_df, included_df,  how='left',
                         left_on='num_paragrafo', right_index=True)

    merged_df.drop(columns=['incluidas_diff'], inplace=True)
    
    merged_df.rename({'paragrafos': 'paragrafo_correspondente'},
                     axis=1, inplace=True)

    return merged_df


#######################################################################
# 4. Outros
#######################################################################

def create_dtm(docs_df):
    '''
        Essa função cria o DTM de todos os textos do corpus.
        Tem como parametro de entrada um dataframe que representa o corpus de textos
    '''

    stopwords = nltk.corpus.stopwords.words('portuguese')
    cv = CountVectorizer(stop_words=stopwords)
    data_cv = cv.fit_transform(docs_df.texto)
    data_dtm = pd.DataFrame(data_cv.toarray(), columns=cv.get_feature_names_out())
    data_dtm.index = docs_df.documento
    return cv, data_dtm

def separar_parrafos_de_clausulas(df2):
    ''' 
        Pega um dataframe com os documentos separados por cláusulas
        e logo divide ele em paragrafos estiquetados por cláusulas
    '''

    Data_Clausulas = pd.DataFrame({'PARAGRAFOS':0,'CLÁUSULA':0},index=[])
    for index in df2.index:
        para_texts = []
        df = pd.DataFrame(df2.iloc[index]).transpose()
        for texts in df['textos']:
            para = texts.split('\n')
            #print(para)
            for p in para:
                #print(p)
                if len(p) == 0:
                    continue
                para_texts.append(p)
        df_cla = pd.DataFrame(para_texts,columns=['PARAGRAFOS'])
        df_cla
        #print(df.clausulas.to_list()[0])
        df_cla['CLÁUSULA'] = df.clausulas.to_list()[0]
        Data_Clausulas = Data_Clausulas.append(df_cla)
        Data_Clausulas.to_pickle('arquivos_pickle/Data_Clausulas_por_paragrafos.pkl')
    return Data_Clausulas


def get_clausulas_clausulasuid(df):
    clausulas = []
    clausulas_uid = []
    for i in range(len(df)):
        clausulas.append(df.at[i, 'clausula'])
        clausulas_uid.append(df.at[i, 'clausula_uid'])
    return clausulas, clausulas_uid


def get_text_info(documento,nome, app):
    try:
        print('entoy aki')
        document_docx = docx.Document(documento)
    
        doc = full_text_extract(documento)
        print('pasé por aki tambien')
        doc_para = doc.split('\n')
        valor_hash = get_hash(os.path.join(
            app.config['DOCX_UPLOADS'], nome))
        id_contrato = uuid4().hex

        return document_docx, doc, doc_para, valor_hash, id_contrato
    except:
        
        return {'message': 'Não foi posivel ler o documento'}, 400


def text_analisis(clausulas, doc_para):
    try:
        # 2.2 Realizando array de similaridade
        similarity_array = similarity(clausulas, doc_para)
        cut = 0.8
        contain_clauses, included_clauses = included(similarity_array, cut)

        # cria listas booleanas das cláusulas iguais, similares e diferentes
        igual, similar, diferente = included_clauses_class(
            similarity_array)

        # cria lista com a posição dos parágrafos
        para_position = get_position(similarity_array)

        # cria lista com os índices de similaridade
        indices = get_indices(similarity_array)

        included_df = pd.DataFrame(
            {'paragrafos': doc_para, 'incluidas_diff': contain_clauses})
        return similarity_array, cut, igual, similar, diferente, para_position, indices, included_df, included_clauses

    except:
        return {'message': 'Não foi posivel realizar a verificação'}, 400


def get_document(app):
    '''Esta função é responsável por pegar o documento que vem dentro da
     requisição e salvá-lo na pasta storage/arcquivos'''
    try:
        if request.files:
            # se foi submetido algum arquivo, salvar ele na pasta storage/arquivos

            documento = request.files['docx']
            documento.save(os.path.join(
                app.config['DOCX_UPLOADS'], documento.filename))
            return documento

        else:
            # caso no foi seleçonado nenhum arquivo
            return {'message': 'Nenhum arquivo seleçõnado'}, 204
    except:
        return {'message': 'Nenhum arquivo seleçõnado'}, 404


def get_list_clausulas(df):
    Row_list = []

    # Iterate over each row
    for index, rows in df.iterrows():
        # Create list for the current row
        my_list = [rows.clausula, rows.indice_diff, rows.resultado,
                   rows.num_paragrafo, rows.paragrafo_correspondente, rows.clausula_uid]

        # append the list to the final list
        Row_list.append(my_list)
    return Row_list


def contract_atributes(document_docx, df):
    # total de cláusulas buscadas
    total_clauses = len(df.index)

    # qtd de cláusulas idênticas encontradas
    n_identical_clauses = len(
        df[df['resultado'] == 'igual'])

    # qtd de cláusulas similares encontradas
    n_similar_clauses = len(
        df[df['resultado'] == 'similar'])

    # qtd de cláusulas não presentes
    n_absent_clauses = len(
        df[df['resultado'] == 'diferente'])

    # define alias de objeto

 

    # dados para salvar na tabela contratos
    dados_contratro = {'total_clausulas': total_clauses, 'num_clausulas_iguais': n_identical_clauses,
                       'num_clausulas_similares': n_similar_clauses, 'num_clausulas_diferentes': n_absent_clauses ,'data_verificacao': datetime.now()}

    return dados_contratro
