from flask_restful import Resource, reqparse
from flask import jsonify, make_response, render_template
from resources.utils import *
from models.models import Clausula
from resources import db


class ClausulasAll(Resource):
    listagem = reqparse.RequestParser()
    listagem.add_argument('clausula', required=True, help='Campo obrigatorio')
    listagem.add_argument('tipo', required=True, help='Campo obrigatorio')

    def get(self):
        try:

        # pegando numero da pagina e quatidade de elementos por cada pagina
            PAGE = int(request.args.get('page', default='1'))
            PER_PAGE = int(request.args.get('per_page', default='5'))

            # Usando SQLAchemy para paginação
            items = Clausula.query.paginate(
                page=PAGE, per_page=PER_PAGE).items
            TOTAL = Clausula.query.paginate(
                page=PAGE, per_page=PER_PAGE).total

            #return{'message':'Erro: Não foi possivel conectar com o banco de dados'},400
            # extraer json dos elementos retornados dp sqlachemy
            DADOS = []
            for i in items:
                DADOS.append(i.json())

            # personalisar a paginação
            clausula_json = get_paginated_list(
                DADOS=DADOS, PAGE=PAGE, PER_PAGE=PER_PAGE, TOTAL=TOTAL)

            return make_response(jsonify(clausula_json))

        except:
            return make_response(jsonify({'message': 'Erro: Não foi possível carregar a listagem de clausulas'}))

    def post(self):
       
        clausula_uid = uuid4().hex
        clausula = request.form.get('clausula', default='', type=str)
        tipo = request.form.get('tipo', default='', type=str)
        new_clausula = Clausula(clausula_uid=clausula_uid, clausula=clausula, tipo=tipo)

        try:
            new_clausula.save_clause()
            return {'message': 'Cláusula salva com sucesso'}, 200
        except:
            return {'message': 'Erro: Não foi possivel salvar a clausula'}, 400



class Clausulas(Resource):

    listagem = reqparse.RequestParser()
    listagem.add_argument('clausula', required=True, help='Campo obrigatorio')
    listagem.add_argument('tipo', required=True, help='Campo obrigatorio')

    def get(self, uid):
       select_id = db.session.query(Clausula).filter(Clausula.clausula_uid == uid).statement
       select_id_df = pd.read_sql_query(select_id, db.engine)
       if not select_id_df.empty:
            select_id_records = select_id_df.to_dict(orient='records')

            # remove dict da lista
            select_id_json = jsonify(select_id_records[0])
            
            return select_id_json

       else:
            return {'message': 'Cláusula não encontrada'}, 404

    def put(self, uid):
        try:
            clausula = request.form.get('clausula', default='', type=str)
            tipo = request.form.get('tipo', default='', type=str)            
            clausula_to_update = Clausula.find_clause(uid)
            
            if clausula_to_update:
                clausula_to_update.update_clause(clausula=clausula, tipo=tipo)
                try:
                    clausula_to_update.save_clause()
                except:
                    return {'message': 'Não foi possivel modificar a clausula'}, 500


                return {'message':'Cláusula modificada com succeso'}, 200
        except:
            return {'message': 'Clusula não encontrada'}, 400


    def delete(self, uid):

        clausula_to_delete = Clausula.find_clause(uid)
        if clausula_to_delete:
           try:
              clausula_to_delete.delete_clause()
           except:
               # Internal Server Error
               return{'message': 'Erro ao tentar apagar a clausula'}, 500
           return{'message': 'A cláusula selecionada foi deletada'}, 200
        return{'message': 'A cláusula selecionada não foi encontrada'}, 404
