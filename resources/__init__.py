from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_cors import CORS

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')

db = SQLAlchemy()
migrate = Migrate()
cors = CORS(app, resources={r"/*": {"origins": "*"}})

from models.models import *
from resources import utils
