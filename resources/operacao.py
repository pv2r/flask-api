from flask_restful import Resource, reqparse
import pandas as pd
from flask import jsonify, make_response
from resources.utils import *
from models.models import Contrato

class Operacao_all(Resource):
    def get(self):

        # recebe parâmetros de filtro
        params = reqparse.RequestParser()
        params.add_argument('emissor', type=str, default='', location='args')
        params.add_argument('nome_guerra', type=str, default='', location='args')
        params = params.parse_args()

        try:
            contracts_df = Contrato.load_contracts()

            operations_df = contracts_df.groupby(['emissor', 'nome_guerra', 'produto']).all()
            operations_df = pd.DataFrame(list(operations_df.index), columns=['emissor', 'nome_guerra', 'produto'])

            if params['emissor'] or params['nome_guerra']:
                operations_df = operations_df[
                    operations_df['emissor'].str.contains(params['emissor'], case=False) &
                    operations_df['nome_guerra'].str.contains(params['nome_guerra'], case=False)]

            if operations_df.empty:
                return {'message':'Não há correspondência de contratos para os filtros buscados'}, 400

            operations_records = operations_df.to_dict(orient='records')

            operations_json = jsonify(get_paginated_df(
                operations_records,
                page=request.args.get('page', default=1, type=int),
                per_page=request.args.get('per_page', default=20, type=int)))  

            return make_response(operations_json)

        except:
            return {'message':'Não foi posivel carregar as operações'}, 400


class Operacao(Resource):
    def get(self, uid_contrato):
        try:
            print('entre aki')
            PAGE = int(request.args.get('page', default='1'))
            PER_PAGE = int(request.args.get('per_page', default='5'))
            contracts_df = Contrato.load_contract(uid_contrato)
            operations_df = contracts_df.groupby(['emissor', 'nome_guerra']).all()
            operations_df = pd.DataFrame(list(operations_df.index), columns=[
                                        'emissor', 'nome_guerra'])
            operations_records = operations_df.to_dict(orient='records')

            operations_json = jsonify(get_paginated_df(
                operations_records,page=PAGE,per_page=PER_PAGE))
            print(operations_json)
            return operations_json

        except:
            return {'message': f'Não foi posivel carregar a operação {uid_contrato}'}, 400
