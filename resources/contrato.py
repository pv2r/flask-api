
from flask_restful import Resource, reqparse
from models.models import Contrato
from resources import db,app
from resources.utils import get_paginated_list, get_document, get_text_info
import flask
import pandas as pd
from flask import jsonify, make_response, request
from datetime import datetime


class Contratos__all(Resource):

    def get(self):
        
        try:
            # recebe parâmetros de filtro
            query_params = reqparse.RequestParser()
            query_params.add_argument('emissor', type=str, default='', location='args')
            query_params.add_argument('nome_guerra', type=str, default='', location='args')
            query_params.add_argument('tipo', type=str, default='', location='args')

            # filtra contratos
            filters = query_params.parse_args()

            query = db.session.query(Contrato)

            if filters['emissor']:
                query = query.filter(Contrato.emissor.contains(filters['emissor']))
            if filters['nome_guerra']:
                query = query.filter(Contrato.nome_guerra.contains(filters['nome_guerra']))
            if filters['tipo']:
                query = query.filter(Contrato.tipo.contains(filters['tipo']))

            page = request.args.get('page', default=1, type=int)
            per_page = request.args.get('per_page', default=20, type=int)

            # carrega arquivos com paginação utilizando sqlalchemy
            items = query.paginate(page=page, per_page=per_page).items
            total = query.paginate(page=page, per_page=per_page).total

            # extrai json dos elementos retornados do sqlachemy
            data = []
            for i in items: 
                data.append(i.json())

            # personalisar paginação
            contracts_json = get_paginated_list(DADOS=data,PAGE=page, PER_PAGE=per_page,TOTAL=total)
            
            return make_response(jsonify(contracts_json))
        
        except:
            return {'message': 'Não foi possível carregar a listagem de contratos'}, 400

    # Post
    def post(self):
        try:
            
            produto = request.form.get('produto', default='', type=str)
            emisor = request.form.get('emisor', default='', type=str)
            nome_guerra = request.form.get('nome_guerra', default='', type=str)
            tipo = request.form.get('tipo', default='', type=str)
            documento = get_document(app=app)
            nome = documento.filename
            
            document_docx, doc, doc_para, valor_hash, id_contrato = get_text_info(documento=documento, nome= nome, app=app)
            # Comprovar se o documento já foi analisado anteriormente
            con = Contrato.find_contract_hash(valor_hash)
            if con:
                return flask.make_response(flask.jsonify(Contrato.query.filter_by(valor_hash=valor_hash).first().json()))

            prop = document_docx.core_properties

            dados = {'contrato_uid': id_contrato, 'valor_hash': valor_hash, 'nome': documento.filename,
                    'produto': produto, 'emissor': emisor, 'nome_guerra': nome_guerra, 'tipo': tipo, 'data_criacao': prop.created,
                    'autor_criacao': prop.author, 'data_modificacao': prop.modified, 'autor_modificacao': prop.last_modified_by,
                    'data_verificacao': None, 'total_clausulas': 0, 'num_clausulas_iguais': 0, 'num_clausulas_similares': 0,
                    'num_clausulas_diferentes': 0}

            contrato = Contrato(**dados)
        # try:
            contrato.save_contract()
        #
            return make_response(jsonify(contrato.json()))
            
        except:
            return {'message':'Erro: Não foi possivel salvar o contrato'},400

class Contratos(Resource):
    listagem = reqparse.RequestParser()
    listagem.add_argument('valor_hash')
    # exemplo de obrigatoriedade de campo
    listagem.add_argument('produto', required=True, help=' Campo Obrigatório')
    listagem.add_argument('nome', required=True, help=' Campo Obrigatório')
    listagem.add_argument('emissor', required=True, help=' Campo Obrigatório')
    listagem.add_argument('nome_guerra', required=True,
                            help=' Campo Obrigatório')
    listagem.add_argument('tipo')
    listagem.add_argument('data_criacao')
    listagem.add_argument('autor_criacao')
    listagem.add_argument('data_modificacao')
    listagem.add_argument('autor_modificacao')
    listagem.add_argument('data_verificacao')

    def get(self, uid):
        try:
            contrato = Contrato.query.filter_by(contrato_uid = uid).first()
        except:
            return {'message':'Erro: Não foi possivel conectar com o banco de dados'}
        if contrato:
            contrato_json = contrato.json()
            return make_response(jsonify(contrato_json))

        else:
            return {'message':'Erro: Contrato não foi encontrado'},400


    # Put
    def put(self, uid):
        dados = Contrato.listagem.parse_args()
        contrato_criado = Contrato.find_contract(uid)
        if contrato_criado:
            contrato_criado.update_contract(uid, **dados)  # Atualizou o contrato
            contrato_criado.save_contract()  # Salvou o contrato
            return contrato_criado.json(), 201  # Contrato Criado ou Atualizado  

        # Caso o contrato não existe faz um post
        contrato = Contrato(uid, **dados)
        try:
            contrato.save_contract()
        except:
                return {'message': 'Erro ao tentar salvar o contrato'}, 500  # Internal Server Error
        return contrato.json, 201  # Criado 

    # Delete
    def delete(self, uid):
        contrato = Contrato.find_contract(uid)
        if contrato:
            try:
                contrato.delete_contract()
            except:
                return{'message': 'Erro ao tentar salvar o contrato'}, 500  # Internal Server Error
            return{'message': 'O contrato selecionado foi deletado'}
        return{'message': 'O contrato selecionado não foi encontrado'}, 404
