
from cmath import isnan
from flask_restful import Resource
import pandas as pd
import docx
from resources.utils import *
from models.models import Contrato, Verificacao

from resources import app, db
from copy import deepcopy


class Save_Sugetions(Resource):
    def get(self):
        id_contrato = request.args.get('contrato_uid', '')
        
        try:
            select_id = db.session.query(Verificacao).filter(
                Verificacao.contrato_uid == id_contrato).statement

            select_id_df = pd.read_sql_query(
                select_id, db.engine).set_index('clausula_uid')

            contrato_query = db.session.query(Contrato).filter(
                Contrato.contrato_uid == id_contrato).statement
            
            nome_contrato = pd.read_sql_query(
                contrato_query, db.engine).at[0, 'nome']
        
        except:
            return {'message':'Erro: Contrato {} não foi encontrado'.format(id_contrato)}, 404
        
        file_path = find_file(
             nome_contrato, app.config['DOCX_UPLOADS'])
    
        doc1 = docx.Document(file_path)

        copy_the_content = deepcopy(doc1)
        try:
            for _,i in select_id_df.iterrows():
                if i.resultado == "alterada":
                    paragraph1 = copy_the_content.paragraphs[int(i.num_paragrafo)]


                    # add a comment on the entire paragrap
                    paragraph1.add_comment(
                        i.paragrafo_modificado, author='Manolisfcb', initials='MM')

                    #copy_the_content = deepcopy(doc1)
                    copy_the_content.save(os.path.join(
                    app.config['ARQUIVOS_MODIF'], 'Revisado_' + nome_contrato))
        except:
            return {'message': 'Erro: Ocurreu um erro inesperado'}, 400

        return {'message': 'sugestão salvada corretamente'}, 200
