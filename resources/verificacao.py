from flask_restful import Resource
from flask import make_response, jsonify
import flask
from resources.utils import *
from models.models import Clausula, Contrato, Verificacao

from resources import app


class Verificacoes(Resource):
    def post(self):
        contrato_uid = request.form['contrato_uid']
        try:
            clauses_df = Clausula.load_clauses()
        except:
            return {'message': 'Não foram carrgadas as clausulas'}, 404

        # Obter lista de clausulas com os uid de cada uma delas
        clausulas, clausulas_uid = get_clausulas_clausulasuid(clauses_df)
        contrato = Contrato.query.filter_by(contrato_uid = contrato_uid).first()
        if contrato == None:
            return {'message':'Erro: Contrato não encontrado'},400
        
        documento = find_file(contrato.nome, app.config['DOCX_UPLOADS'])

        if contrato.total_clausulas != 0:
            return {'contrato_uid': f'{contrato_uid}', 'message': 'Verificação já foi feita anteriormente'}

        try:
            # Obter informação do documento apos de leitura dele
            document_docx, _doc, doc_para, _valor_hash, _id_contrato = get_text_info(
                documento=documento, nome = contrato.nome, app=app)
        except:
            a = Verificacao_uid.get(self, uid=contrato_uid)
            return a

        # Realizar comparação de similaridade
        similarity_array, cut, igual, similar, diferente, para_position, indices, included_df, included_clauses = text_analisis(
            clausulas, doc_para)

        # Criando dataframe de similaridade
        merged_df = build_clauses_df(included_df=included_df, clauses=clausulas, included_clauses=included_clauses, id_contrato=contrato_uid,
                                     igual=igual, similar=similar, clausulas_uid=clausulas_uid, diferente=diferente, para_position=para_position, indices=indices)

        merged_df['paragrafo_modificado'] = np.nan

        # Criando dataframe de verificação para salvar no banco de dados (tabela verificação)
        verificacao_db = merged_df[['contrato_uid', 'clausula_uid', 'indice_diff', 'resultado',
                                    'num_paragrafo', 'paragrafo_correspondente', 'paragrafo_modificado']]

        
        # Obter Metadados do contrato
        dados_contratro = contract_atributes(document_docx=document_docx, df=merged_df)

        # Obter clausula analisada com paragrafo, indice de similaridade e indice do paragrafo
        Row_list = get_list_clausulas(merged_df)
        
        # Salvando nas tabelas de verificacao e contratos do banco de dados

        contrato_to_db = Contrato.query.filter_by(contrato_uid=contrato_uid).first()
        print(contrato_to_db)
        contrato_to_db.update_contract_verifications(**dados_contratro)
        contrato_to_db.save_contract()



        verificacao_db.to_sql('verificacao', app.config['SQLALCHEMY_DATABASE_URI'],
                                  if_exists='append', index=False)

        return flask.make_response(flask.jsonify({'contrato_uid': contrato_to_db.contrato_uid, "message": "Verificação realizada com sucesso"}))

    def get(self):
        try:
            return flask.make_response(flask.render_template('public/upload_docx.html'))
        except:
            return {'message':'404 URL não foi encontrado'}, 404

    def put(self):

        id_contrato= request.args.get('contrato_uid', '')
        id_clausula = request.args.get('clausula_uid', '')
        text = request.args.get('text', default='qwertyuiolkçklçklçklçklçklçp', type=str)
       

        verificacao = Verificacao.find_verification(
                contrato_uid=id_contrato, clausula_uid = id_clausula)

        if verificacao == None: 
            return {'message':'Erro: clausula e/ou contrato não encontrados'}, 400
        try:
            verificacao.update_verification(resultado='alterada', paragrafo_modificado = text)
            verificacao.save_verification()
        except:
            return{'message': 'Erro: Não foi possivel salvar a modificação realisada'}, 400
        return {'message': 'Sucesso: sugestão salvada corretamente'}, 200


class Verificacao_uid(Resource):

    def get(self,uid):
        try:
            contrato = Contrato.query.filter_by(contrato_uid=uid).first()
            obj = {}
            obj['autor_criacao'] = contrato.autor_criacao
            obj["autor_modificacao"] = contrato.autor_modificacao
            obj["contrato_uid"] = contrato.contrato_uid
            obj["data_criacao"] = contrato.data_criacao
            obj["data_modificacao"] = contrato.data_modificacao
            obj["data_verificacao"] = contrato.data_verificacao
            obj["nome_contrato"] = contrato.nome
            obj["num_clausulas_buscadas"] = contrato.total_clausulas
            obj["num_clausulas_iguais"] = contrato.num_clausulas_iguais
            obj["num_clausulas_nao_encontradas"] = contrato.num_clausulas_diferentes
            obj["num_clausulas_similares"] = contrato.num_clausulas_similares
            obj["produto"] = contrato.produto

            clausulas = []
            verificacoes = Verificacao.query.filter_by(contrato_uid=uid).all()

            for verificacao in verificacoes:

                paragrafo_correspondente = verificacao.paragrafo_correspondente

                paragrafo_modificado = verificacao.paragrafo_modificado
                num_paragrafo = verificacao.num_paragrafo
                resultado = verificacao.resultado
                indice_diff = verificacao.indice_diff
                clausula_uid = verificacao.clausula_uid
                clausula = Clausula.query.filter_by(
                    clausula_uid=clausula_uid).first().clausula
                comparacao = [clausula, indice_diff,
                            resultado, num_paragrafo, paragrafo_correspondente, paragrafo_modificado]
                clausulas.append(comparacao)
            obj["clausulas"] = clausulas

            return make_response(jsonify(obj))
        except:
            return make_response(jsonify({'message':'Erro: Verificação não encontrada'}))
