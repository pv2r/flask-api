FROM python:3.8-alpine
WORKDIR /app
COPY . /app
RUN apk add --no-cache --update \
    gcc \
    gfortran musl-dev g++ \
    libffi-dev openssl-dev \
    libxml2 libxml2-dev \
    libxslt libxslt-dev \
    libjpeg-turbo-dev zlib-dev \
    libpq

RUN pip install --upgrade cython \
    pip install --no-cache --upgrade pip && \
    pip --no-cache-dir install -r requirements.txt

ENTRYPOINT ["python3", "app.py"]

