from base import session
from tables import Listagem
import uuid
import random
import datetime


# gera datetime aleatório
def generate_datetime():
    return datetime.datetime(random.randint(2019, 2021), random.randint(1, 12), random.randint(1, 28))

# cria objeto Listagem
nova_versao = Listagem(
    valor_hash=uuid.uuid4().hex,
    nome_da_versao='arquivo1_v1',
    produto='Debênture',
    emissor='TIBAGI ENERGIA SPE S.A.',
    nome_guerra='TIBA',
    data_de_criacao=generate_datetime(),
    autor_criacao='autor1',
    data_modificacao=generate_datetime(),
    autor_modificacao='revisor1')
    
# adiciona e faz commit da versão
with session as s:
    s.add(nova_versao)
    s.commit()

# gera número aleatório de 1 a 3
n_123 = [random.randint(1, 3) for i in range(2, 11)]

# cria objetos Listagem
lista_de_versoes = [Listagem(
    valor_hash=uuid.uuid4().hex,
    nome_da_versao=f'arquivo{i}_v{n_123[i-2]}',
    produto='Debênture',
    emissor='TIBAGI ENERGIA SPE S.A.',
    nome_guerra='TIBA',
    data_de_criacao=generate_datetime(),
    autor_criacao=f'autor_criacao{n_123[i-2]}',
    data_modificacao=generate_datetime(),
    autor_modificacao=f'revisor{n_123[i-2]}') for i in range(2, 11)]

# adiciona e faz commit de todos os documentos
with session as s:
    s.add_all(lista_de_versoes)
    s.commit()