import sqlalchemy as db


connection_string = 'mysql+pymysql://root:123456@localhost:3306'
engine = db.create_engine(connection_string)

with engine.connect() as connection:
    engine.execute('DROP DATABASE contratos;')