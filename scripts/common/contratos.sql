-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: contratos
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clausula`
--

DROP TABLE IF EXISTS `clausula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clausula` (
  `clausula_uid` varchar(64) NOT NULL,
  `clausula` varchar(3060) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clausula_uid`),
  UNIQUE KEY `clausula_uid` (`clausula_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clausula`
--

LOCK TABLES `clausula` WRITE;
/*!40000 ALTER TABLE `clausula` DISABLE KEYS */;
INSERT INTO `clausula` VALUES ('245a76a1a3164bc1b44b00d4b2729e76','Os Contratos de Garantia (conforme definido na Cláusula 4.15.1 abaixo), assim como quaisquer aditamentos subsequentes a estes contratos, serão celebrados e levados a registro nos competentes Cartórios de Registro de Títulos e Documentos, conforme indicado nos respectivos instrumentos, sendo certo que os Contratos de Garantia, incluindo respectivos aditamentos deverão ser apresentados para registro no prazo determinado no respectivo instrumento, devendo ser fornecida ao Agente Fiduciário, dentro de até 5 (cinco) Dias Úteis contados da data do respectivo registro, 1 (uma) via original do respectivo instrumento devidamente registrado.',NULL),('345f4bf37817432f8b1b461c8e9bb027','A presente Escritura de Emissão e os Contratos de Garantia (conforme definido na Cláusula 4.15.1 abaixo) são firmados pela Emissora com base nas deliberações (i) da Assembleia Geral Extraordinária de acionistas da Emissora, realizada em 9 de agosto de 2019 (“AGE da Emissora”) e (ii) na Reunião do Conselho de Administração da Emissora, realizada em 9 de agosto de 2019 (“RCA da Emissora” e, em conjunto com a AGE da Emissora, as “Aprovações da Emissora”), nas quais foram deliberadas: (a) a aprovação da Emissão e da Oferta Restrita (conforme definidos na Cláusula II abaixo), bem como seus termos e condições; (b) a outorga das garantias a serem constituídas por meio do Contrato de Cessão Fiduciária (conforme definido na Cláusula 4.15.1, item (ii) abaixo), bem como a celebração deste último instrumento e do Contrato de Alienação Fiduciária (conforme definido na Cláusula 4.15.1, item (i) abaixo); (c) a contratação das Fianças Bancárias (conforme definidas na Cláusula 4.16.2 abaixo); e (d) a autorização à diretoria da Emissora para praticar todos e quaisquer atos e assinar todos e quaisquer documentos necessários à implementação e formalização das deliberações das Aprovações da Emissora, especialmente para realização da Oferta Restrita e da Emissão, incluindo esta Escritura de Emissão e seus aditamentos, bem como ratificação de todos e quaisquer atos até então praticados e todos e quaisquer documentos até então assinados pela diretoria da Emissora para a implementação da Oferta Restrita, da Emissão e da constituição das garantias necessárias.',NULL),('71a15ea3f2874e60a8026af6bae65270','TIBAGI ENERGIA SPE S.A., sociedade por ações de capital fechado, com sede na Cidade de Belo Horizonte, Estado de Minas Gerais, na Avenida Getúlio Vargas, nº 874, 10º andar, Sala 1006, inscrita no Cadastro Nacional de Pessoa Jurídica do Ministério da Economia (“CNPJ”) sob o nº 23.080.281/0001-35 e na Junta Comercial do Estado de Minas Gerais (“JUCEMG”) sob o NIRE nº 31.3.00112209, neste ato representada por seu(s) representante(s) legal(is) devidamente autorizado(s) e identificado(s) nas páginas de assinaturas do presente instrumento (“Emissora” ou “Companhia”); e',NULL),('cbb372ed662f446bae2c3ff57457ef85','Eu sou uma clásula não presente.',NULL);
/*!40000 ALTER TABLE `clausula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contrato` (
  `contrato_uid` varchar(64) NOT NULL,
  `valor_hash` varchar(64) DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `produto` varchar(255) DEFAULT NULL,
  `emissor` varchar(255) DEFAULT NULL,
  `nome_guerra` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `data_criacao` datetime DEFAULT NULL,
  `autor_criacao` varchar(255) DEFAULT NULL,
  `data_modificacao` datetime DEFAULT NULL,
  `autor_modificacao` varchar(255) DEFAULT NULL,
  `data_verificacao` datetime DEFAULT NULL,
  `total_clausulas` int DEFAULT NULL,
  `num_clausulas_iguais` int DEFAULT NULL,
  `num_clausulas_similares` int DEFAULT NULL,
  `num_clausulas_diferentes` int DEFAULT NULL,
  PRIMARY KEY (`contrato_uid`),
  UNIQUE KEY `contrato_uid` (`contrato_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
INSERT INTO `contrato` VALUES ('0a0a34ae428e40f29df091e8d90ad8bf','f6b39ba77eb644e7b5a36f5fdf892d35','arquivo10_v2','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA','Termo de Securitização','2019-03-05 00:00:00','Manuel Alejandro','2020-06-26 00:00:00','Manuel Alejandro','2021-02-11 00:00:00',2,2,0,0),('0bd192a2e8ef489aae12061fa2309bd4','931e64175ed54b1d899b0531f6f82fb7','arquivo5_v1','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Termo de Securitização','2019-12-26 00:00:00','Felipe Silveira','2021-01-18 00:00:00','Manuel Alejandro','2021-02-27 00:00:00',1,1,0,0),('0ef1097b6a244de39d9ab714acb226cf','fc5a69099c7742758c1978c726050899','arquivo16_v3','Debênture','Banco John Deere','JD','Escritura de Emissão','2021-06-04 00:00:00','Viviane Nonato','2021-11-12 00:00:00','Felipe Silveira','2020-08-04 00:00:00',4,0,2,2),('1e7f654135a44affb2cc379f251a3275','605c8bd3261a4d98bb7c259067a3ae6c','arquivo13_v3','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Termo de Securitização','2021-06-27 00:00:00','Manuel Alejandro','2020-11-22 00:00:00','Felipe Silveira','2021-10-08 00:00:00',3,0,1,2),('2926fb954a5c45fd8c214b1d7abb3def','0c4e2ddb85ba424da7d5544c6a1b198f','arquivo11_v2','Debênture','Banco John Deere','JD','Termo de Securitização','2020-06-04 00:00:00','Manuel Alejandro','2019-12-16 00:00:00','Manuel Alejandro','2019-05-11 00:00:00',4,2,2,0),('2f97fe141a88444ea860e525f220cd39','50823e3cc12f4a5faba90bdc21990564','arquivo20_v1','Debênture','Banco John Deere','JD','Escritura de Emissão','2019-07-10 00:00:00','Viviane Nonato','2020-12-05 00:00:00','Viviane Nonato','2020-05-21 00:00:00',1,1,0,0),('3a17c6d1e2e340c780b2866b814ef21d','256de4906cec45ef9a6a10196ac627bb','arquivo18_v1','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Termo de Securitização','2020-11-14 00:00:00','Felipe Silveira','2020-06-24 00:00:00','Manuel Alejandro','2019-06-03 00:00:00',3,0,1,2),('58592d02d4f94bd19d1298e424350034','a8acbbf0e61f4f018b5eb006ee9a3bdb','arquivo19_v1','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA','Escritura de Emissão','2020-02-16 00:00:00','Manuel Alejandro','2021-05-05 00:00:00','Felipe Silveira','2019-06-11 00:00:00',3,1,1,1),('85cf3cd937e54d9693f36b9446737ea9','a4ffb3eafbcb4b15939c1d7c96a155d8','arquivo4_v1','Debênture','Banco John Deere','JD','Escritura de Emissão','2019-12-09 00:00:00','Felipe Silveira','2021-04-12 00:00:00','Felipe Silveira','2020-07-27 00:00:00',0,0,0,0),('9da1ef56a61f48aeb69f4bc9d920f5e9','63cc4beabedf45aebbcf1f84dcc9fe30','arquivo17_v1','Debênture','AES Tietê Energia S.A.','AES','Termo de Securitização','2020-06-25 00:00:00','Felipe Silveira','2020-06-26 00:00:00','Viviane Nonato','2019-01-02 00:00:00',2,1,1,0),('a09060cef9fd431690ce1532bf380f62','0219648e844a42ebbdc995b6ad376c15','arquivo12_v2','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Escritura de Emissão','2020-10-25 00:00:00','Felipe Silveira','2020-04-12 00:00:00','Felipe Silveira','2019-12-01 00:00:00',5,2,1,2),('a37260b5e0ac41dfaa0866dfb3916bc5','a9b50b2a721042b3a87287e27ce88f9a','arquivo8_v1','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Escritura de Emissão','2020-06-10 00:00:00','Manuel Alejandro','2019-07-07 00:00:00','Viviane Nonato','2021-02-27 00:00:00',2,2,0,0),('a54d596f2e0f40778a9819b3fc643342','5be7bb78d6e64b91abb2ac34f7c40ec9','arquivo6_v3','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA','Escritura de Emissão','2019-06-17 00:00:00','Manuel Alejandro','2019-10-15 00:00:00','Viviane Nonato','2019-01-16 00:00:00',5,2,1,2),('a65b63d6a840478fb00375bd1455bd0b','8b47223dc3f44b5a9c49ac5b97f2e4a2','arquivo7_v3','Debênture','Banco John Deere','JD','Escritura de Emissão','2021-05-13 00:00:00','Viviane Nonato','2020-11-03 00:00:00','Felipe Silveira','2021-04-23 00:00:00',1,1,0,0),('b41f8c55d0ec4a34b6cc9a8c96497117','98cc6251933d4a2fb0d22f089cfba842','arquivo3_v2','Debênture','Banco John Deere','JD','Escritura de Emissão','2019-11-25 00:00:00','Felipe Silveira','2019-01-27 00:00:00','Felipe Silveira','2021-09-22 00:00:00',5,1,2,2),('bf6de7c1344a49e6996ad3f87bd8caf8','639b963bd9dd4c05a8e200bd2e4d8191','arquivo15_v2','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA','Termo de Securitização','2019-08-28 00:00:00','Manuel Alejandro','2020-07-13 00:00:00','Manuel Alejandro','2019-03-21 00:00:00',1,0,0,1),('c2e36b0a42f7427ca7ebc8d44df77a5b','95e4e8378123482381189ecc40fce888','arquivo1_v3','Debênture','AES Tietê Energia S.A.','AES','Escritura de Emissão','2020-02-24 00:00:00','Felipe Silveira','2021-06-16 00:00:00','Viviane Nonato','2019-06-14 00:00:00',5,1,2,2),('d152098625df419a8827d761954bb7b4','ce7810cc15844eea8a1039210a4c03c2','arquivo14_v2','Debênture','Banco John Deere','JD','Termo de Securitização','2019-03-28 00:00:00','Viviane Nonato','2020-06-17 00:00:00','Manuel Alejandro','2021-09-02 00:00:00',4,2,0,2),('f3ad6a50003643b78100fad2ac879c19','301e9ba632df4d47b0fa2e1979daec65','arquivo9_v3','Debênture','AES Tietê Energia S.A.','AES','Escritura de Emissão','2019-05-15 00:00:00','Felipe Silveira','2020-03-21 00:00:00','Viviane Nonato','2021-11-12 00:00:00',1,0,0,1),('f78d7a71dd85420f8eeb8cea0317b8d7','66b5d3c8aba0409cbed3de553eba53b4','arquivo2_v1','Debênture','TIBAGI ENERGIA SPE S.A.','TIBA OP2','Escritura de Emissão','2021-04-08 00:00:00','Felipe Silveira','2021-08-03 00:00:00','Felipe Silveira','2020-09-16 00:00:00',3,0,1,2);
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verificacao`
--

DROP TABLE IF EXISTS `verificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `verificacao` (
  `verificacao_uid` int NOT NULL AUTO_INCREMENT,
  `contrato_uid` varchar(64) DEFAULT NULL,
  `clausula_uid` varchar(64) DEFAULT NULL,
  `indice_diff` float DEFAULT NULL,
  `resultado` varchar(64) DEFAULT NULL,
  `num_paragrafo` int DEFAULT NULL,
  `paragrafo_correspondente` varchar(3060) DEFAULT NULL,
  `paragrafo_modificado` varchar(3060) DEFAULT NULL,
  PRIMARY KEY (`verificacao_uid`),
  UNIQUE KEY `verificacao_uid` (`verificacao_uid`),
  KEY `contrato_uid` (`contrato_uid`),
  KEY `clausula_uid` (`clausula_uid`),
  CONSTRAINT `verificacao_ibfk_1` FOREIGN KEY (`contrato_uid`) REFERENCES `contrato` (`contrato_uid`),
  CONSTRAINT `verificacao_ibfk_2` FOREIGN KEY (`clausula_uid`) REFERENCES `clausula` (`clausula_uid`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verificacao`
--

LOCK TABLES `verificacao` WRITE;
/*!40000 ALTER TABLE `verificacao` DISABLE KEYS */;
INSERT INTO `verificacao` VALUES (1,'c2e36b0a42f7427ca7ebc8d44df77a5b','71a15ea3f2874e60a8026af6bae65270',0.9,'similar',4,'Texto do parágrafo 4',NULL),(2,'c2e36b0a42f7427ca7ebc8d44df77a5b','345f4bf37817432f8b1b461c8e9bb027',0.8,'similar',16,'Texto do parágrafo 16',NULL),(3,'c2e36b0a42f7427ca7ebc8d44df77a5b','cbb372ed662f446bae2c3ff57457ef85',0.3,'diferente',NULL,NULL,NULL),(4,'c2e36b0a42f7427ca7ebc8d44df77a5b','245a76a1a3164bc1b44b00d4b2729e76',1,'igual',20,'Texto do parágrafo 20',NULL);
/*!40000 ALTER TABLE `verificacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-23 11:52:57
