from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import declarative_base


connection_string = 'mysql+pymysql://root:123456@localhost:3306/contratos'
engine = create_engine(connection_string, echo=True)

Session = sessionmaker(bind=engine)
Session.configure(bind=engine)
session = Session()

Base = declarative_base()