from sqlalchemy import Column, Integer, Float, String, DateTime, ForeignKey
from base import Base
import datetime


class Verificacao(Base):
    __tablename__ = 'verificacao'
    
    verificacao_uid = Column(Integer(), primary_key=True, unique=True)
    contrato_uid = Column(String(64), ForeignKey('contrato.contrato_uid'))
    clausula_uid = Column(String(64), ForeignKey('clausula.clausula_uid'))
    indice_diff = Column(Float())
    resultado = Column(String(64))
    num_paragrafo = Column(Integer())
    paragrafo_correspondente = Column(String(3060))
    paragrafo_modificado = Column(String(3060))


class Contrato(Base):
    __tablename__ = "contrato"

    contrato_uid = Column(String(64), primary_key=True, unique=True)
    valor_hash = Column(String(64))
    nome = Column(String(255))
    produto = Column(String(255))
    emissor = Column(String(255))
    nome_guerra = Column(String(255))
    tipo = Column(String(255))
    data_criacao = Column(DateTime)
    autor_criacao = Column(String(255))
    data_modificacao = Column(DateTime)
    autor_modificacao = Column(String(255))
    data_verificacao = Column(DateTime, default=datetime.datetime.utcnow())
    total_clausulas = Column(Integer())
    num_clausulas_iguais = Column(Integer())
    num_clausulas_similares = Column(Integer())
    num_clausulas_diferentes = Column(Integer())


class Clausula(Base):
    __tablename__ = "clausula"

    clausula_uid = Column(String(64), primary_key=True, unique=True)
    clausula = Column(String(3060))
    tipo = Column(String(255))
