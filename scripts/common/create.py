import sqlalchemy as db
from tables import Base


connection_string = 'mysql+pymysql://root:123456@localhost:3306'
engine = db.create_engine(connection_string)

with engine.connect() as connection:
    engine.execute('CREATE DATABASE IF NOT EXISTS contratos;')
    engine.execute('USE contratos;')

Base.metadata.create_all(engine)