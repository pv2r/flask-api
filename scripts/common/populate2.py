from base import session
from tables import Listagem
import uuid
import random
import datetime


# gera datetime aleatório
def generate_datetime():
    return datetime.datetime(random.randint(2019, 2021), random.randint(1, 12), random.randint(1, 28))

# gera número aleatório de 1 a 3
n_123 = [random.randint(1, 3) for i in range(0, 26)]

# cria objetos Listagem
lista_de_versoes1 = [Listagem(
    valor_hash=uuid.uuid4().hex,
    nome_da_versao=f'arquivo{i}_v{n_123[i-2]}',
    produto='Debênture',
    emissor='TIBAGI ENERGIA SPE S.A.',
    nome_guerra='TIBA OP2',
    data_de_criacao=generate_datetime(),
    autor_criacao=f'autor_criacao{n_123[i-2]}',
    data_modificacao=generate_datetime(),
    autor_modificacao=f'revisor{n_123[i-2]}') for i in range(11, 16)]

# cria mais objetos Listagem
lista_de_versoes2 = [Listagem(
    valor_hash=uuid.uuid4().hex,
    nome_da_versao=f'arquivo{i}_v{n_123[i-2]}',
    produto='Debênture',
    emissor='Banco John Deere',
    nome_guerra='JD',
    data_de_criacao=generate_datetime(),
    autor_criacao=f'autor_criacao{n_123[i-2]}',
    data_modificacao=generate_datetime(),
    autor_modificacao=f'revisor{n_123[i-2]}') for i in range(16, 21)]

# cria mais objetos Listagem
lista_de_versoes3 = [Listagem(
    valor_hash=uuid.uuid4().hex,
    nome_da_versao=f'arquivo{i}_v{n_123[i-2]}',
    produto='Debênture',
    emissor='AES Tietê Energia S.A.',
    nome_guerra='AES',
    data_de_criacao=generate_datetime(),
    autor_criacao=f'autor_criacao{n_123[i-2]}',
    data_modificacao=generate_datetime(),
    autor_modificacao=f'revisor{n_123[i-2]}') for i in range(21, 26)]

# adiciona e faz commit de todos os documentos
with session as s:
    s.add_all(lista_de_versoes1)
    s.add_all(lista_de_versoes2)
    s.add_all(lista_de_versoes3)
    s.commit()