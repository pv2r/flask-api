# %%
import pandas as pd
import uuid
import random
import datetime
import os
import numpy as np

# %% [markdown]
# ## Definições de path

# %%
current_path = os.path.dirname(os.path.abspath(__file__))

base_path = os.path.abspath(os.path.join(current_path, os.pardir))

debentures_data_path = os.path.join(base_path, 'resources', 'debentures_data')

clauses_path = os.path.join(debentures_data_path, 'clauses.csv')

# %% [markdown]
# ## contratos_df

# %%
def generate_number():
    return random.randint(1, 3)

# %%
def generate_company_op():
    return random.choice([
        ('TIBAGI ENERGIA SPE S.A.', 'TIBA'),
        ('TIBAGI ENERGIA SPE S.A.', 'TIBA OP2'),
        ('Banco John Deere', 'JD'),
        ('AES Tietê Energia S.A.', 'AES')])

# %%
def generate_datetime():
    return datetime.datetime(random.randint(2019, 2021), random.randint(1, 12), random.randint(1, 28))

# %%
def generate_name():
    return random.choice(['Felipe Silveira', 'Manuel Alejandro', 'Viviane Nonato'])

# %%
def contract_generator(n=10):

    rows = []
    for i in range(1, n+1):
        company_op = generate_company_op()
        
        id_contrato = i
        valor_hash = uuid.uuid4().hex
        nome_da_versao = f'arquivo{i}_v{generate_number()}'
        produto = 'Debênture'
        emissor = company_op[0]
        nome_guerra = company_op[1]
        data_de_criacao = generate_datetime()
        autor_criacao = generate_name()
        data_modificacao = generate_datetime()
        autor_modificacao = generate_name()

        rows.append([id_contrato, valor_hash, nome_da_versao, produto, emissor, nome_guerra, data_de_criacao, autor_criacao, data_modificacao, autor_modificacao])

    return rows


# %%
contracts_columns = ['id_contract', 'valor_hash', 'contract_name', 'product', 'issuer',
                     'operation', 'created_at', 'author', 'modified_at', 'modified_by']

# %%
contratos_df = pd.DataFrame(contract_generator(
    10), columns=contracts_columns).set_index('id_contract')

# %%
def insert_contract(contratos_df):
    company_op = generate_company_op()
    i = len(contratos_df.index) + 1

    id_contract = i
    valor_hash = uuid.uuid4().hex
    contract_name = f'arquivo{i}_v{generate_number()}'
    produt = 'Debênture'
    issuer = company_op[0]
    operation = company_op[1]
    created_at = generate_datetime()
    author = generate_name()
    modified_at = generate_datetime()
    modified_by = generate_name()

    row = [id_contract, valor_hash, contract_name, produt, issuer, operation,
           created_at, author, modified_at, modified_by]
    row_df = pd.DataFrame(
        [row], columns=contracts_columns).set_index('id_contract')
    return pd.concat([contratos_df, row_df])

# %%
# contratos_df = insert_contract(contratos_df)
# contratos_df

# %% [markdown]
# ## clausulas_df

# %%


def csv_to_df(path):
    '''
        Cria lista e dataframe de cláusulas a partir do arquivo csv
    '''

    # carrega arquivo em lista de cláusulas
    with open(path, encoding="utf8") as file:
        clauses = [line.rstrip("\n") for line in file]

    # cria dataframe de cláusulas
    clauses_df = pd.DataFrame({'clauses': clauses})
    return clauses, clauses_df


# %%
clauses_columns = ['id_clause', 'clause', 'clause_type']

# %%
clauses, _ = csv_to_df(clauses_path)

# %%
clauses_id = [i+1 for i in range(len(clauses))]

# %%
clausulas_df = pd.DataFrame({'clause': clauses, 'id_clause': clauses_id, 'id_contrct': 1}, columns=clauses_columns).set_index('id_clause')

# %% [markdown]
# ## verificacoes_df

# %%
verifications_columns = ['id_clause', 'id_contract', 'index_diff',
                         'result', 'paragraph_number', 'corresponding_paragraph', 'modified_paragraph']

# %%
def verification_generator(n=2):
    verifications_id = []
    for i in range(1, n+1):
        verifications_id.extend([i] * len(clauses))

    clauses_id_cross_verifications = clauses_id * n

    contracts_id = []
    for i in range(n):
        contracts_id.extend([random.choice(list(contratos_df.index))] * len(clauses))

    return verifications_id, clauses_id_cross_verifications, contracts_id

# %%
verifications_id, clauses_id_cross_verifications, contracts_id = verification_generator(1)

# %%
verificacoes_df = pd.DataFrame({'id_clause': clauses_id_cross_verifications,
                               'id_contract': contracts_id}, columns=verifications_columns)  # .set_index('id_clausula')

# %%
bloco = {'index_diff': [0.9, 0.8, 0.3, 1.0], 'result': ['similar', 'similar', 'diferente', 'igual'], 'paragraph_number': [
    4, 16, None, 20], 'corresponding_paragraph': ['Texto do parágrafo 4', 'Texto do parágrafo 16', None, 'Texto do parágrafo 20']}

# %%
bloco_df = pd.DataFrame(bloco).fillna(value=np.nan)

# %%
verificacoes_df.loc[0:3, ['index_diff', 'result',
                          'paragraph_number', 'corresponding_paragraph']] = bloco_df

# %%
def insert_verification(verificacoes_df, id_contrato, bloco_df):

    id_clausula = clauses_id
    id_contrato = [id_contrato] * len(clauses)

    row = [id_clausula, id_contrato]
    columns = ['id_clause', 'id_contract']

    row_df = pd.DataFrame([row], columns=columns)
    row_dict = {
                'id_clause': id_clausula,
                'id_contract': id_contrato}

    new_index_start = verificacoes_df.index.stop
    new_index_stop = verificacoes_df.index.stop + len(clauses)
    new_index = list(range(new_index_start, new_index_stop))
    bloco_df.index = new_index

    row_df = pd.DataFrame(row_dict).fillna(value=np.nan)

    row_df = pd.concat([verificacoes_df, row_df], ignore_index=True)

    row_df.loc[new_index_start:new_index_stop, ['index_diff',
                                                'result', 'paragraph_number', 'corresponding_paragraph']] = bloco_df

    return row_df

# %%
# def insert_verification(verificacoes_df, id_contrato, indice_diff, resultado, paragrafo_correspondente, paragrafo_correspondente):
#     id_verificacao = [verificacoes_df['id_verificacao'].max() + 1] * len(clauses)

#     id_clausula = clauses_id

#     id_contrato = [id_contrato] * len(clauses)

#     row = [id_verificacao, id_clausula, id_contrato, indice_diff, resultado, paragrafo_correspondente, paragrafo_correspondente]
#     columns = ['id_verificacao', 'id_clausula', 'id_contrato', 'indice_diff', 'resultado', 'paragrafo_correspondente', 'paragrafo_correspondente']

#     row_df = pd.DataFrame([row], columns=columns)
#     row_dict = {'id_verificacao': id_verificacao,
#                 'id_clausula': id_clausula,
#                 'id_contrato': id_contrato,
#                 'indice_diff': [0.9, 0.8, 0.3, 1.0],
#                 'resultado': ['similar', 'similar', 'diferente', 'igual'],
#                 'paragrafo_correspondente': [4, 16, None, 20],
#                 'paragrafo_correspondente': ['Texto do parágrafo 4', 'Texto do parágrafo 16', None, 'Texto do parágrafo 20']}

#     row_df = pd.DataFrame(row_dict).fillna(value=np.nan)
    
#     return pd.concat([verificacoes_df, row_df], ignore_index=True)   

# %%
# verificacoes_df = insert_verification(verificacoes_df, id_contrato=7, indice_diff=[0.9, 0.8, 0.3, 1.0], resultado=['similar', 'similar', 'diferente', 'igual'], paragrafo_correspondente= [4, 16, None, 20], paragrafo_correspondente=['Texto do parágrafo 4', 'Texto do parágrafo 16', None, 'Texto do parágrafo 20'])
# verificacoes_df

# %% [markdown]
# ## Mege contratos_df e clausulas_df

# %%
contratos_clausulas = pd.merge(verificacoes_df, clausulas_df, left_on='id_clause', right_index=True)
contratos_clausulas.sort_values('id_contract')


