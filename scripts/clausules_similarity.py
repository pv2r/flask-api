from scripts.utils import *
from os.path import exists
import pandas as pd
import pickle
import numpy as np


def similarity(df,clauses):
    clauses = clauses
    docs_df = df
    
    text_index = 0 # define o índice do texto
    text = docs_df['paragrafos'].iloc[text_index] # pega o texto do índice definido

    # calcula a matriz de similaridade por diff das cláusulas em cada parágrafo do texto definido
    similarity_array = similarities(clauses, text)
    np.set_printoptions(linewidth=150, edgeitems=5)

    # mostra quais cláusulas estão presentes no texto com similaridade acima do corte
    cut = 0.8
    included,n_lines = included_clauses(similarity_array)

    # mostra quantas cláusulas estão presentes no texto
    print('\n Número de cláusulas presentes no texto: '  + str(sum(included)))

    # cria dataframe cláusulas incluídas
    included_df = pd.DataFrame({'paragrafos': text, 'incluidas_diff': included})
    clausulas_df = pd.DataFrame({'clausula': clauses, 'incluidas_diff': n_lines})
    #print(clausulas_df)

    #print('\n')
    # salva dataframe das cláusulas incluídas arquivo pickle
    included_df.to_pickle('arquivos_pickle/included_df.pkl')
    clausulas_df.to_pickle('arquivos_pickle/clausulas_df.pkl')

    included_selection_df = included_df[included_df['incluidas_diff'] == True]
    #print(included_selection_df)
    


    # salva dataframe da seleção das cláusulas incluídas em arquivo pickle
    included_selection_df.to_pickle('arquivos_pickle/included_selection_df.pkl')

    print('\n')
    if (len(included_selection_df) == len(clausulas_df )):
        print('Verificação de cláusulas concluida, Não foram encontrados inconcistencias no contrato')
    else:
        print('A(s) clausula(s) não foram encontradas:')
        print(clausulas_df[clausulas_df['incluidas_diff']==False]['clausula'])
    return included_selection_df, clausulas_df