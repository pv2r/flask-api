import os
import hashlib
import docx
import re
import string
import unicodedata
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import difflib
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import nltk
import pandas as pd


#######################################################################
# 1. Extração
#######################################################################

def get_text_name(folder):
    '''
        Recebe o caminho de um diretório e retorna uma lista com os nomes dos arquivos de extensão .docx ordenados
    '''

    only_docx = [f for f in os.listdir(folder) if f.lower().endswith('.docx')]
    only_docx.sort()
    return only_docx


def get_text_dir(folder):
    '''
        Recebe o caminho de um diretório e retorna uma lista com os caminhos dos arquivos de extensão .docx ordenados
    '''

    only_docx_path = [os.path.join(folder, f) for f in os.listdir(folder) if f.lower().endswith('.docx')]
    only_docx_path.sort()
    return only_docx_path


def get_hash(file_dir, chunk_size=1024):
    '''
        Recebe o caminho de um arquivo e retorna o hash acumulado
    '''

    hash = hashlib.sha256()
    with open(file_dir, "rb") as f:
        # lê o primeiro bloco do arquivo
        chunk = f.read(chunk_size)
        # fica lendo arquivo até o fim e atualiza o hash
        while chunk:
            hash.update(chunk)
            chunk = f.read(chunk_size)

    # Returna hex checksum
    return hash.hexdigest()


def para_text_extract(files):
    '''
        Extrai docs, parágrafos docx e textos separados em parágrafos docx e '\n'
    '''

    docs = [docx.Document(doc) for doc in files]
    texts = [doc.paragraphs for doc in docs]

    para_texts = []
    for text in texts:
        para_group = []
        for para in text:
            para_text_normalized = unicodedata.normalize('NFKC', para.text)
            para_group.extend(para_text_normalized.split('\n'))
        para_texts.append(para_group)

    return docs, texts, para_texts



def text_info(para_texts):
    '''
        Extrai número de documentos, tamanho de cada documento e tamanho total dos documentos
    '''

    n_docs = len(para_texts)
    docs_len = [len(para) for para in para_texts]
    total_len = sum(docs_len)

    return n_docs, docs_len, total_len


def date_extract(docs):
    '''
        Extrai datas dos cabeçalhos
    '''

    dates = []
    for doc in docs:
        section = doc.sections[0]
        header = section.first_page_header
        try:
            date = header.paragraphs[1].text
        except IndexError:
            date = None
        dates.append(date)

    return dates


def full_text_extract(filename):
    '''
        Extrai texto completo
    '''

    doc = docx.Document(filename)

    full_text = []
    for para in doc.paragraphs:
        full_text.append(para.text)

    return '\n'.join(full_text)


def combine_text(list_of_strings):
    '''
        Une lista de strings com caractere espaço
    '''

    combined_text = ' '.join(list_of_strings)
    return combined_text


def csv_to_df(path):
    '''
        Cria lista e dataframe de cláusulas a partir do arquivo csv
    '''

    with open(path) as f:
        clauses = []
        for line in f:
            clauses.append(line.rstrip("\n"))

    # cria dataframe das cláusulas
    clauses_df = pd.DataFrame({'clausulas': clauses})
    return clauses, clauses_df


#######################################################################
# 2. Pré-processamento
#######################################################################

def clean_text_round1(text):
    '''
        Elimina pontuação, colchetes e palavras que contribuam pouco
    '''

    text = text.lower()
    text = re.sub('\[.*?\]', '', text)
    text = re.sub('[%s]' %re.escape(string.punctuation),'', text)
    text = re.sub('\w*\d\w*', '', text)
    # remove palavras com menos de 3 caracteres
    text = re.sub(r'\b\w{1,2}\b', '', text)
    return text


def clean_text_round2(text):
    '''
        Segunda rodada de limpeza
    '''
    
    text = re.sub('[‘’“”…]', '', text)
    text = re.sub('\n', '', text)
    text = re.sub('\d{2}.\d{3}.\d{3}/\d{4}-\d{2}','"CNPJ"', text)
    text = re.sub('\(s\)','', text)
    text = re.sub('\(\w{2}\)','', text)
    return text


def tokenize(para_text):
    '''
        Tokeniza em palavras, coloca em minúsculas, mantém apenas tokens alfanuméricos e remove stopwords a partir de lista de parágrafos.
        Para ser usada com pd.DataFrame(df['coluna'].apply(tokenize))
    Arg:
        Lista de parágrafos
    Return:
        Lista de lista de tokens
    '''
    stops = stopwords.words('portuguese')
    stops.append('s')

    para_group = []
    for para in para_text:
        lower_tokens = word_tokenize(para.lower())
        alnum_tokens = [t for t in lower_tokens if t.isalnum()]
        no_stops = [t for t in alnum_tokens if t not in stops]
        para_group.append(no_stops)

    return para_group


def para_tokenize(paragraph):
    stops = stopwords.words('portuguese')
    stops.append('s')

    lower_tokens = word_tokenize(paragraph.lower())
    alnum_tokens = [t for t in lower_tokens if t.isalnum()]
    no_stops = [t for t in alnum_tokens if t not in stops]

    return no_stops


#######################################################################
# 3. Processamento
#######################################################################

def similarity(clauses, text):
    '''
        Calcula a similaridade entre uma lista de cláusulas e uma lista de segmentos de texto e
        retorna um array com o índice de similaridade entre cada cláusula e cada segmento
    Args:
        clauses - lista de strings contendo cláusulas obrigatórias
        text - lista de parágrafos de um texto
    Return:
        Array com índices de similaridade entre cada cláusula e cada segmento
    '''

    similarity_indices = []
    for clause in clauses:
        similarity_group = []
        for para in text:
            ratio = difflib.SequenceMatcher(None, clause, para).ratio()
            similarity_group.append(ratio)
        similarity_indices.append(similarity_group)
    return np.array(similarity_indices)


# cria alias da função similarity para compatibilidade
similarities = similarity


def included(similarity_indices, cut=0.8):
    '''
        Gera uma tupla de listas indicando se cada parágrafo contém uma das cláusulas
        e se cada cláusula está presente no texto
    Arg:
        similarity_indices - Lista com os índices de similaridade da clásula em cada parágrago do texto
    Return:
        lines - Lista indicando se cada parágrafo contém uma das cláusulas
        columns - Lista se cada cláusula está presente no texto
    '''

    mask = similarity_indices >= cut
    lines = np.any(mask, axis=0)
    columns = np.any(mask, axis=1)

    return lines, columns


# função para compatibilidade (remover após revisar)
def included_clauses(similarity_indices, cut=0.8):
    '''
        Gera uma lista indicando se as cláusulas estão presentes em cada parágrafo do texto
    Arg:
        similarity_indices - Lista com os índices de similaridade da clásula em cada parágrago do texto
    Return:
        Lista com 0s e 1s indicando se a cláusula está presente em cada parágrafo do texto
    '''

    mask = similarity_indices >= cut
    #print(mask)
    n_lines = np.any(mask,axis=1)
    lines = np.any(mask, axis=0)
    return lines, n_lines


def included_clauses_class(similarity_indices, cut=0.8):
    mask_igual = (similarity_indices == 1)
    mask_similar = np.logical_and(similarity_indices >= cut, similarity_indices < 1)
    mask_diferente = similarity_indices < cut
    igual = np.any(mask_igual, axis=1)
    similar = np.any(mask_similar, axis=1)
    diferente = np.alltrue(mask_diferente, axis=1)

    return igual, similar, diferente


def get_position(similarity_indices, cut=0.8):
    para_position = []
    for i in similarity_indices:
        line = np.where(i >= cut)
        if line[0].size > 0:
            para_position.append(line[0][0])
        else:
            para_position.append(None)

    return para_position


def get_indices(similarity_indices):
    max_similarity_indices = []
    for i in similarity_indices:
        line = np.max(i)
        max_similarity_indices.append(line)

    return max_similarity_indices


def build_clauses_df(included_df, clauses, included_clauses, igual, similar, diferente, para_position, indices):
    clausulas_df = pd.DataFrame({'clausula': clauses,
                                'incluidas_diff': included_clauses,
                                'igual': igual,
                                'similar': similar,
                                'diferente': diferente,
                                'paragrafo': para_position,
                                'indice_diff': indices})
    clausulas_df['resultado'] = pd.Series(np.where(clausulas_df.loc[:,["igual", "similar", "diferente"]].any(axis=1), clausulas_df.loc[:,["igual", "similar", "diferente"]].idxmax(axis=1), np.nan), name="resultado")
    clausulas_df.drop(columns=['incluidas_diff', 'igual', 'similar', 'diferente'], inplace=True)
    clausulas_df
    merged_df = pd.merge(clausulas_df, included_df,  how='left', left_on='paragrafo', right_index=True)
    merged_df.drop(columns='incluidas_diff', inplace=True)
    merged_df.rename({'paragrafos': 'paragrafo_correspondente'}, axis=1, inplace=True)

    return merged_df


#######################################################################
# 4. Outros
#######################################################################

def create_dtm(docs_df):
    '''
        Essa função cria o DTM de todos os textos do corpus.
        Tem como parametro de entrada um dataframe que representa o corpus de textos
    '''

    stopwords = nltk.corpus.stopwords.words('portuguese')
    cv = CountVectorizer(stop_words=stopwords)
    data_cv = cv.fit_transform(docs_df.texto)
    data_dtm = pd.DataFrame(data_cv.toarray(), columns=cv.get_feature_names_out())
    data_dtm.index = docs_df.documento
    return cv, data_dtm

def separar_parrafos_de_clausulas(df2):
    ''' 
        Pega um dataframe com os documentos separados por cláusulas
        e logo divide ele em paragrafos estiquetados por cláusulas
    '''

    Data_Clausulas = pd.DataFrame({'PARAGRAFOS':0,'CLÁUSULA':0},index=[])
    for index in df2.index:
        para_texts = []
        df = pd.DataFrame(df2.iloc[index]).transpose()
        for texts in df['textos']:
            para = texts.split('\n')
            #print(para)
            for p in para:
                #print(p)
                if len(p) == 0:
                    continue
                para_texts.append(p)
        df_cla = pd.DataFrame(para_texts,columns=['PARAGRAFOS'])
        df_cla
        #print(df.clausulas.to_list()[0])
        df_cla['CLÁUSULA'] = df.clausulas.to_list()[0]
        Data_Clausulas = Data_Clausulas.append(df_cla)
        Data_Clausulas.to_pickle('arquivos_pickle/Data_Clausulas_por_paragrafos.pkl')
    return Data_Clausulas


import spacy
import random
#from spacy import util
from spacy.tokens import Doc
from spacy.training import Example
from spacy.language import Language

def print_doc_entities(_doc: Doc):
    if _doc.ents:
        for _ent in _doc.ents:
            print(f"     {_ent.text} {_ent.label_}")
    else:
        print("     NONE")

def customizing_pipeline_component(nlp: Language):
    import spacy
    import random
    #from spacy import util
    from spacy.tokens import Doc
    from spacy.training import Example
    from spacy.language import Language
    ''' Customizar as entidades do Spacy '''
    TRAIN_DATA = [
    ("neste ato representada por Thais GLAUBER GADWEYERS RG o' 3562284 e CPF o' 257.752.156-02", [(27,50,'PER'),(74,88,"CPF")]),
    ("HENRIQUE LUIZ HELEODORO DA SILVA, RG 8686 CREA-DF e CPF nO 391.352.504-10, resolvem celebrar o presente contrato.",[(2,34,'PER'),(59,73,"CPF")]),
    ("Manuel naceu o 27 de novembro do 2021 em Fortaleza e o CPF dele é 445.225.854-56",[(0,6,"PER"),(41,50,'LOC'),(66,80,"CPF")]),
    ("O sonho dela é ir para Australia visitar seu irmão", [(25,34,"LOC")]),
    ("Açmira é uma otima pessoa, eu gosto muito dela",[(0,6,"PER")]),
    ('Luis naceu o 27/14/1993 em Fortaleza',[(0,4,'PER'), (13,23,'DATA'),(27,36,'LOC')]),
    ('Roberta De Sousa Ramos portadora do documento de identificação RG nº 5.397.740-3 SSP/SP e CPF/MF sob nº 012.608.578-16',[(0,22,'PER'),(81,87,'LOC'),(104,118,'CPF')]),
    ('Conselho de Administração da Emissora, (i) em reunião realizada em 21 de julho de 2017 (“RCA 21.07.2017”), aprovou a emissão das Debêntures ',[(67,86,'DATA'),(93,103,'DATA') ]),
    ('''automaticamente dispensada do registro de distribuição de que trata o artigo 19 da Lei nº 6.385, de 7 de dezembro de 1976''',[(83,95,'Lei'),(100,121,'DATA')]),
    ('Nos termos do artigo 59, caput, e 122, IV, da Lei nº 6.404, de 15 de dezembro de 1976, conforme em vigor',[(47,59,'Lei'),(64,86,'DATA')]),
    ('Oliveira Trust Ditribuidora de Títulos e Valores Mobiliários S.A., instituição financeira autorizada a exercer as funções de agente fiduciário, com sede na Cidade e Estado do Rio de Janeiro, na Avenida das Américas LOC , nº 3.434, Bloco 7, Sala 201',[(175,189,"LOC"),(194, 243,'LOC')]),
    ('Certifico registro sob o nº 6338127 em 05/10/2017 da Empresa CENTRO DE IMAGEM DIAGNOSTICOS S/A',[(61, 94,'ORG'),(39, 49, 'DATA')]),
    ('automaticamente dispensada do registro de distribuição de que trata o artigo 19 da Lei nº 6.385, de 7 de dezembro de 1976',[(83, 95,'Lei'),(100, 121,'DATA')]),
    ('''Marcelle Motta Santoro, brasileira, solteira, advogada, inscrita na OAB/RJ sob o nº 185.511, inscrita no CPF/MF sob o nº 109.809.047.06,
    com domicílio na Cidade do Rio de Janeiro, Estado do Rio de Janeiro, na Avenida das Américas,
    nº 4.200, bloco 8, ala B, salas 302''',[(0, 22,'PER'),(121, 135,'CPF'),(164, 178,'LOC'),(180, 204,'LOC'),(209, 266,'LOC')]),
    ('''em 01 de abril de 2021, a Direcional Engenharia S.A. (“Direcional”) celebrou com a
    Securitizadora o “Instrumento Particular de Escritura da 7ª Emissão de Debêntures Simples''',[(3,22,'Data'),(26, 51,'ORG')]),
    ('''com valor nominal total de R$ 200.000.000,00 (duzentos milhões de reais), na Data de Emissão,''',[(28, 45,'Dinhero')]),
    ('Valor total da emissão do Crédito Imobiliário: R$ 251.585.114,00 (duzentos milhões de reais), na Data de Emissão das Debêntures',[(47,64,'Dinhero')]),
    ('O dólar abre a sessão desta sexta-feira (5) em queda de 0,56%, a R$ 5,574, com investidores monitorando o cenário fiscal brasileiro.',[(28,39,"DATA"),(65,73,'Dinhero')]),
    ('A Minerva Foods registrou lucro líquido de R$ 72,4 milhões no terceiro trimestre de 2021.',[(2, 16,'ORG'),(43, 58,'Dinhero'),(62, 89,'DATA')]),
    ('A Embraer registrou um prejuízo líquido de R$ 234,2 milhões no terceiro trimestre, reduzindo perdas em relação ao mesmo período do ano passado',[(2, 9,'ORG'),(43, 59,'Dinhero'),(63, 81,'DATA'),(131, 142,'DATA')]),
    ('Certifico registro sob o nº 6338127 em 05/10/2017 da Empresa CENTRO DE IMAGEM DIAGNOSTICOS S/A',[(61, 94,'ORG'),(39, 49,'DATA')]),
    ('''A Emissão será realizada na forma do artigo 2º da Lei nº 12.431,
    de 24 de junho de 2011, conforme alterada (“Lei 12.431”) e do Decreto nº 8.874,
    de 11 de outubro de 2016 (“Decreto 8.874”), da Resolução do Conselho Monetário Nacional (“CMN”) n° 3.947, de 27 de janeiro de 2011, conforme alterada(“Resolução CMN 3.947”),
    ou de normas posteriores que as alterem, substituam ou complementem, tendo em 
    vista o enquadramento do Projeto (conforme definido abaixo) como prioritário pelo
    Ministério de Minas e Energia (“MME”), por meio da Portaria do MME: nº 257,
    de 11 de setembro de 2017, publicada no Diário Oficial da União em 14 de setembro de 2017
    (“Portaria de Enquadramento”).''',[(37, 63,'Lei'),(69, 88,'DATA'),(110, 120,'Lei'),(127, 144,'Lei'),(150, 171,'DATA'),(174, 187,'DATA'),(207, 242,'ORG'),(256, 277,'DATA'),(485, 522,'ORG'),(565, 587,'DATA'),(629, 651,'DATA')]),









    ]

    # resultado antes do treino 
    print(f"\nAntes de treinar:")
    doc = nlp(u'Luis naceu o 27 de novembro de 2021 em Fortaleza e o CPF dele é 241.718.958-55.')
    print_doc_entities(doc)

    # desactivando todos os pipe menos o 'ner'
    disabled_pipes = []
    for pipe_name in nlp.pipe_names:
        if pipe_name != 'ner':
            nlp.disable_pipes(pipe_name)
            disabled_pipes.append(pipe_name)

    print("   Trinando ...")
    optimizer = nlp.create_optimizer()
    for _ in range(50):
        random.shuffle(TRAIN_DATA)
        for raw_text, entity_offsets in TRAIN_DATA:
            doc = nlp.make_doc(raw_text)
            example = Example.from_dict(doc, {"entities": entity_offsets})
            nlp.update([example], sgd=optimizer)

    # activando os pipe desactivados
    for pipe_name in disabled_pipes:
        nlp.enable_pipe(pipe_name)

    # depois do treino
    print(f"Depois de treinar:")
    doc = nlp(u'Luis naceu o 27 de novembro de 2021 em Fortaleza e o CPF dele é 241.718.958-55.')
    print_doc_entities(doc)
    return nlp
