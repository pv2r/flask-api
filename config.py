import os


class Config(object):
    DEBUG = False
    TESTING = False

    SECRET_KEY = "xxx"

    DB_NAME = " production_contratos_db"
    DB_USERNAME = 'root'
    DB_PASSWORD = 'senhateste'
    DOCX_UPLOADS = 'leitura_de_contratos_api\\resources\storage\\arquivos' #caminho no servidor em que vão ser guardados os arquivos
    ARQUIVOS_PERMITIDOS = ["DOCX", "docx", "Docx"] # extenções permitidas para fazer upload

    SESSION_COOKIE_SECURE =True

class ProducctionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True
    HOST = '0.0.0.0'
    PORT = 5000

    DB_NAME = "contratos"
    DB_USERNAME = 'root'
    DB_PASSWORD = '123456'

    # caminhos padrão
    BASE_PATH = os.path.dirname(os.path.abspath(__file__))
    RESOURCES_PATH = os.path.join(BASE_PATH, 'resources')
    DEBENTURE_DATA_PATH = os.path.join(BASE_PATH, 'resources', 'debentures_data')

    # caminho para salvar os arquivos
    DOCX_UPLOADS = os.path.join(RESOURCES_PATH, 'storage', 'arquivos')
    ARQUIVOS_MODIF = os.path.join(RESOURCES_PATH, 'storage', 'arquivos_modificados')
    ARQUIVOS_PERMITIDOS = ["DOCX", "docx", "Docx"]  # extenções permitidas para fazer upload

    SESSION_COOKIE_SECURE = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:123456@localhost:3306/contratos'
    SQLALCHEMY_TRACK_MODIFICATIONS = False


class TestingConfig(Config):
    TESTING = True
    
    DB_NAME = "contratos_db"
    DB_USERNAME = 'root'
    DB_PASSWORD = 'senhateste'
    SESSION_COOKIE_SECURE = False
